<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/NotaDAO.php";
class Nota{
    private $idEstudiante;
    private $idCurso;
    private $nota;
   
    private $conexion;
    private $notaDAO;
    
    public function getIdEstudiante(){
        return $this -> idEstudiante;
    }
    
    public function getIdCurso(){
        return $this -> idCurso;
    }
    
    public function getNota(){
        return $this -> nota;
        
    }
    
    
    public function Nota($idEstudiante = "", $idCurso = "", $nota = ""){
        $this -> idEstudiante = $idEstudiante;
        $this -> idCurso = $idCurso;
        $this -> nota = $nota;

        $this -> conexion = new Conexion();
        $this -> notaDAO = new NotaDAO($this -> idEstudiante, $this -> idCurso, $this -> nota);
    }

    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> notaDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> notaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idCurso = $resultado[0];
        $this -> nota = $resultado[1];
        $this -> precio = $resultado[2];
        $this -> imagen = $resultado[3];
    }
    
    
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> notaDAO -> consultarTodos());
        $cursos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Nota($resultado[0], $resultado[1], $resultado[2]);
            array_push($cursos, $c);
        }
        $this -> conexion -> cerrar();        
        return $cursos;
    }
    
  
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> notaDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> notaDAO -> editar());
        $this -> conexion -> cerrar();
    }
  
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> notaDAO -> consultarFiltro($filtro));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Nota($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    
}

?>