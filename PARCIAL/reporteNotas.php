<?php
require "fpdf/fpdf.php";
require_once "logica/Nota.php";
ob_end_clean(); //    the buffer and never prints or returns anything.
ob_start(); // it starts buffering

$nota = new Nota();
$notas = $nota->consultarTodos();

$pdf = new FPDF("P", "mm", "Letter");
$pdf->SetFont("Arial", "B", 20);
$pdf->AddPage();
$pdf->SetXY(0, 0);

$pdf->Cell(216, 15, "Calificaciones", 0, 2, "C");


$pdf->SetFont("Courier", "B", 10);
$pdf->SetFont('Arial', 'B', 12);

$pdf->Ln();



$pdf->Cell(30, 12, "Estudiante", 0, 0, 'C', 0);
$pdf->Cell(30, 12, "Curso", 0, 0, 'C', 0);
$pdf->Cell(40, 12, "Nota", 0, 0, 'C', 0);




$pdf->Ln();
$i = 1;
foreach ($notas as $notaActual) {


    $pdf->SetFillColor(255, 255, 255);
    $pdf->SetTextColor(40, 40, 40);
    $pdf->SetDrawColor(240, 240, 240);
    $pdf->SetFont('Arial', '', 12);
 
    $pdf->Cell(30, 20, $notaActual->getIdEstudiante(), 1, 0, 'C', 1);
    $pdf->Cell(30, 20, $notaActual->getIdCurso(), 1, 0, 'C');
    $pdf->Cell(30, 20, $notaActual->getNota(), 1, 0, 'C');
    
    

    $pdf->Ln();
    $i++;
}

$pdf->Output();
ob_end_flush(); // It's printed here, because ob_end_flush "prints" what's in
              // the buffer, rather than returning it
              //     (unlike the ob_get_* functions)