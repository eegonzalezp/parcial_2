<?php

$estudiante = new Estudiante($_GET["idEstudiante"]);
$estudiante ->consultar();
?>
<?php
$nombre = $estudiante->getNombre();

if (isset($_POST["nombre"])) {
	$nombre = $_POST["nombre"];
}

$apellido = $estudiante->getApellido();

if (isset($_POST["apellido"])) {
	$apellido = $_POST["apellido"];
}

$curso = "";
if (isset($_POST["curso"])) {
	$curso = $_POST["curso"];
}

if (isset($_POST["editar"])) {


	$nota = $_POST["nota"];
	
	$n = new Nota($nombre, $curso, $nota);
	$n->editar();
}
?>

<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-header text-center text-white bg-dark">
					<h4>Calificación</h4>
				</div>
				<div class="card-body">
					<form method="post">
						<div class="row">
							<div class="col-10">
								<div class="form-group">

									<input type="text" name="nombre" class="form-control" placeholder="Nombre" value="<?php echo $nombre ?>" required>
								</div>
								<div class="form-group">
									<input type="text" name="curso" class="form-control" placeholder="Curso" value="<?php echo $apellido ?>" required>
								</div>
								<div class="form-group">
									<input type="number" name="nota" class="form-control" placeholder="Nota" value="<?php echo $nota ?>" required>
								</div>
								<button type="submit" name="editar" class="btn btn-success btn-block">Registrar</button>
							</div>


						</div>

					</form>

				</div>
			</div>
		</div>
	</div>
</div>