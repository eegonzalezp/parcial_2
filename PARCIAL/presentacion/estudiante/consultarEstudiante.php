<?php
$estudiante = new Estudiante();
$estudiantes = $estudiante -> consultarTodos();

$curso = new Curso();
$cursos = $curso -> consultarTodos();
?>

<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Estudiantes</h4>
				</div>
				<div class="text-right"><?php echo count($estudiantes) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Notas</th>
							<th>Reporte</th>
							
						</tr>
						<?php 
						$i=1;
						foreach($estudiantes as $estudianteActual){
						    echo "<tr>";
							
							echo "<td>" . $estudianteActual -> getIdEstudiante() . "</td>";
						    echo "<td>" . $estudianteActual -> getNombre() . "</td>";
							echo "<td>" . $estudianteActual -> getApellido() . "</td>";
                            
                            echo "<td> <a href='index.php?pid=". base64_encode("presentacion/estudiante/notaEstudiante.php") . "&idEstudiante=" . $estudianteActual -> getIdEstudiante()."' data-toggle='tooltip' data-placement='left' title='Editar'><span class='fas fa-book'></span></a> </td>";
                            echo "<td> <a href='index.php?pid=". base64_encode("reporteNotas.php") . "&idEstudiante=" . $estudianteActual -> getIdEstudiante()."' data-toggle='tooltip' data-placement='left' title='Editar'><span class='fas fa-book'></span> </a> </td>";
						    echo "</tr>";
						    $i++;
						}
                        ?>
                       
					</table>
				</div>
            </div>
		</div>
	</div>
</div>